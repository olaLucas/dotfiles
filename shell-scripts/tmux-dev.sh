#!/usr/bin/bash
cd $1
workspace=$(fzf --walker=dir)
distrobox enter dev -- \
  tmux new-session -t "dev" -c $workspace
