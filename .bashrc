#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

### NNN cd on quit
nnn ()
{
    # Block nesting of nnn in subshells
    [ "${NNNLVL:-0}" -eq 0 ] || {
        echo "nnn is already running"
        return
    }

    # The behaviour is set to cd on quit (nnn checks if NNN_TMPFILE is set)
    # If NNN_TMPFILE is set to a custom path, it must be exported for nnn to
    # see. To cd on quit only on ^G, remove the "export" and make sure not to
    # use a custom path, i.e. set NNN_TMPFILE *exactly* as follows:
    #      NNN_TMPFILE="${XDG_CONFIG_HOME:-$HOME/.config}/nnn/.lastd"
    export NNN_TMPFILE="${XDG_CONFIG_HOME:-$HOME/.config}/nnn/.lastd"

    # Unmask ^Q (, ^V etc.) (if required, see `stty -a`) to Quit nnn
    # stty start undef
    # stty stop undef
    # stty lwrap undef
    # stty lnext undef

    # The command builtin allows one to alias nnn to n, if desired, without
    # making an infinitely recursive alias
    command nnn "$@"

    [ ! -f "$NNN_TMPFILE" ] || {
        . "$NNN_TMPFILE"
        rm -f -- "$NNN_TMPFILE" > /dev/null
    }
}

export TERM=xterm-256color
export EDITOR=nvim
export VISUAL=nvim


PS1='\u@\h \w \$ '


# script calls
alias conf="exec ~/shell-scripts/config-files.sh"

TMUX_DEV_PATH="~/shell-scripts/tmux-dev.sh"
JUST_CODING_PATH="~/git-repos/just-coding/"
alias dev="exec $TMUX_DEV_PATH ~/git-repos/"
alias rsdev="exec $TMUX_DEV_PATH $JUST_CODING_PATH/rust/"
alias pydev="exec $TMUX_DEV_PATH $JUST_CODING_PATH/py/"
alias cdev="exec $TMUX_DEV_PATH $JUST_CODING_PATH/C/"

# using tree instead of ls
alias tl="tree -lhagpf"

# managing cpu
alias info-frequency="sudo cpupower frequency-info"
alias set-frequency="sudo cpupower frequency-set -u"
alias base-freq="sudo cpupower frequency-set -u 1400Mhz"

# programming lang
alias py="python3"
alias rs="rustc"
alias cg="cargo"

# terminal commands
alias cl='clear && fastfetch'
alias ll='lsd -lha'
alias ls='lsd --color=auto'
alias grep='grep --color=auto'

# xmodmap
alias xmmp="~/.xmmp.sh"

# package manager
alias pac="sudo pacman"
alias syu="sudo pacman -Syu"

# editing i3
alias edi3='$EDITOR ~/.config/i3/config'

# editing .vimrc
alias ednvim='$EDITOR ~/.config/nvim/init.lua'

# tmux aliases
alias tk='tmux lsk -N|less'

# cargo aliases
alias cb='cargo clippy -- -Wclippy::nursery -Wclippy::pedantic && cargo build'
alias cr='cargo run'


# Set up fzf key bindings and fuzzy completion
eval "$(fzf --bash)"

# starship 
eval "$(starship init bash)"

# i use arch btw
fastfetch
. "$HOME/.cargo/env"
