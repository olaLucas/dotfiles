#!/bin/bash
# ABNT Layout
# xmodmap -e "keycode 66 = Mode_switch" &&
# xmodmap -e "keysym j = j J Left" &&
# xmodmap -e "keysym k = k K Down" &&
# xmodmap -e "keysym l = l L Up" &&
# xmodmap -e "keysym ccedilla = ccedilla Ccedilla Right"

# US Layout
xmodmap -e "keycode 66 = Mode_switch" &&
xmodmap -e "keysym j = j J Left" &&
xmodmap -e "keysym k = k K Down" &&
xmodmap -e "keysym l = l L Up" &&
xmodmap -e "keysym semicolon = semicolon colon Right"  
